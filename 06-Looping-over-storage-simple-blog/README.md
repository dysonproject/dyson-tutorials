# step 0
Let's start where we left off [05-Making-it-pretty-with-html-and-css](/02-Making-it-pretty-with-html-and-css/README.md).
```python
import html
from string import Template
from dys import _chain, get_script_address, get_caller


def say_hello(my_name: str):
    assert get_script_address() == get_caller(), "permission denied"
    data = "hello " + my_name
    print(data)  # to help debug
    return _chain(
        "dyson/sendMsgCreateStorage",
        creator=get_script_address(),
        index=get_script_address() + "/my_greeting",
        data=data,
        force=True,
    )


def get_greeting():
    return _chain(
        "dyson/QueryStorage",
        index=get_script_address() + "/my_greeting",
    )


def render_page(body: str):
    return Template(
        """<!doctype html>
    <html>
        <head>
            <title>Hello World</title>
            <meta charset=utf-8>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
            <style>
                .heading {
                    text-shadow: #FC0 1px 0 10px;
                }
            </style>
        </head>
        <body>
            <div class="container">
                <h1 class="heading">Greeting!</h1>
                <p>$body</p>
            </div>
        </body>
    </html>
    """
    ).safe_substitute(body=body)


def application(environ, start_response):
    start_response("200 ok", [("Content-type", "text/html")])
    greeting_data = get_greeting()["result"]["storage"]["data"]
    return [
        render_page(html.escape(greeting_data)).encode(),
    ]
```
# step 1
Let's rename some functions and variable to be more intuitive. We will be storing posts so let's not use the phrase "greeting" anymore.
```diff
@@ -3,9 +3,9 @@
 from dys import _chain, get_script_address, get_caller
 
 
-def say_hello(my_name: str):
+def new_post(body: str):
     assert get_script_address() == get_caller(), "permission denied"
-    data = "hello " + my_name
+    data = body
     print(data)  # to help debug
     return _chain(
         "dyson/sendMsgCreateStorage",
@@ -16,7 +16,7 @@
     )
 
 
-def get_greeting():
+def get_post():
     return _chain(
         "dyson/QueryStorage",
         index=get_script_address() + "/my_greeting",
@@ -51,7 +51,7 @@
 
 def application(environ, start_response):
     start_response("200 ok", [("Content-type", "text/html")])
-    greeting_data = get_greeting()["result"]["storage"]["data"]
+    post_data = get_post()["result"]["storage"]["data"]
     return [
-        render_page(html.escape(greeting_data)).encode(),
+        render_page(html.escape(post_data)).encode(),
     ]

```
# step 2
Now we will update `new_post` to accept a title as well, and join that with the body using f-strings.
```diff
@@ -3,9 +3,9 @@
 from dys import _chain, get_script_address, get_caller
 
 
-def new_post(body: str):
+def new_post(title: str, body: str):
     assert get_script_address() == get_caller(), "permission denied"
-    data = body
+    data = f"{title} -- {body}"
     print(data)  # to help debug
     return _chain(
         "dyson/sendMsgCreateStorage",

```
# step 3
Refactor the `get_post` function to return the data directly, so we don't have to the nested dictionary lookup. This makes it easer to make changes in the future.
```diff
@@ -17,10 +17,11 @@
 
 
 def get_post():
-    return _chain(
+    response = _chain(
         "dyson/QueryStorage",
         index=get_script_address() + "/my_greeting",
     )
+    return response["result"]["storage"]["data"]
 
 
 def render_page(body: str):
@@ -51,7 +52,7 @@
 
 def application(environ, start_response):
     start_response("200 ok", [("Content-type", "text/html")])
-    post_data = get_post()["result"]["storage"]["data"]
+    post_data = get_post()
     return [
         render_page(html.escape(post_data)).encode(),
     ]

```
# step 4
This step is a big change. Now, `new_post` no longer stores the data at the hardcoded index `[SCRIPT_ADDRESS]/my_greeting`.
Now it stores it at a dynamic index that is based on a sanitized version of the `title`. We sanitize the `title` with [re.sub()](https://docs.python.org/3/library/re.html#re.sub) by replacing 
all the non-word characters with dashes "-".

We also change the query from `QueryStorage` to `QueryPrefixStorage` which returns all Storage items who's `index` begins with the supplied `prefix`.
Specifically we want all storage of this script that starts with `/post/v1/`. By using "v1" we can version our data schemes which will save us a headache later when we must do data migrations.

Also to help the develpment we are returning all the post data as a `str` for now.

At this point you should test making new posts and seeing the raw data change in the browser.

```diff
@@ -1,3 +1,4 @@
+import re2
 import html
 from string import Template
 from dys import _chain, get_script_address, get_caller
@@ -6,22 +7,24 @@
 def new_post(title: str, body: str):
     assert get_script_address() == get_caller(), "permission denied"
     data = f"{title} -- {body}"
+    title_slug = re2.sub("\W+", "-", title)
     print(data)  # to help debug
     return _chain(
         "dyson/sendMsgCreateStorage",
         creator=get_script_address(),
-        index=get_script_address() + "/my_greeting",
+        index=get_script_address() + "/post/v1/" + title_slug,
         data=data,
         force=True,
     )
 
 
-def get_post():
+def get_all_posts():
     response = _chain(
-        "dyson/QueryStorage",
-        index=get_script_address() + "/my_greeting",
+        "dyson/QueryPrefixStorage", 
+        prefix=get_script_address() + "/post/v1/"
     )
-    return response["result"]["storage"]["data"]
+    # str to help development
+    return str(response["result"])
 
 
 def render_page(body: str):
@@ -52,7 +55,7 @@
 
 def application(environ, start_response):
     start_response("200 ok", [("Content-type", "text/html")])
-    post_data = get_post()
+    post_data = get_all_posts()
     return [
         render_page(html.escape(post_data)).encode(),
     ]

```
# step 5
Now that we have seen the raw data being rendered in the browser, it's possible to make something that renders it in a more pleasing way.
We will make a new function `render_posts` that will render the posts in an list. We also remove the temporary `str` helper.

```diff
@@ -23,8 +23,7 @@
         "dyson/QueryPrefixStorage", 
         prefix=get_script_address() + "/post/v1/"
     )
-    # str to help development
-    return str(response["result"])
+    return response["result"]
 
 
 def render_page(body: str):
@@ -53,9 +52,17 @@
     ).safe_substitute(body=body)
 
 
+def render_posts(posts: list[dict[str, str]]):
+    return (
+        "<ul>"
+        + "".join(["<li>" + html.escape(s["data"]) + "</li>" for s in posts])
+        + "</ul>"
+    )
+
+
 def application(environ, start_response):
     start_response("200 ok", [("Content-type", "text/html")])
     post_data = get_all_posts()
     return [
-        render_page(html.escape(post_data)).encode(),
+        render_page(render_posts(post_data["storage"])).encode(),
     ]

```
# step 6
Finally let's rename the title to be `Posts` to match the rest of our great web site.

```diff
@@ -43,7 +43,7 @@
         </head>
         <body>
             <div class="container">
-                <h1 class="heading">Greeting!</h1>
+                <h1 class="heading">Posts!</h1>
                 <p>$body</p>
             </div>
         </body>

```

# Conclusion

In this post we learned:
1. How to use `dyson/QueryPrefixStorage` to get all Storage for a script
2. To loop over a list of Storage and render each post in a list

Try customizing your page with more css and have fun with it!

```python
import re2
import html
from string import Template
from dys import _chain, get_script_address, get_caller


def new_post(title: str, body: str):
    assert get_script_address() == get_caller(), "permission denied"
    data = f"{title} -- {body}"
    title_slug = re2.sub("\W+", "-", title)
    print(data)  # to help debug
    return _chain(
        "dyson/sendMsgCreateStorage",
        creator=get_script_address(),
        index=get_script_address() + "/post/v1/" + title_slug,
        data=data,
        force=True,
    )


def get_all_posts():
    response = _chain(
        "dyson/QueryPrefixStorage", 
        prefix=get_script_address() + "/post/v1/"
    )
    return response["result"]


def render_page(body: str):
    return Template(
        """<!doctype html>
    <html>
        <head>
            <title>Hello World</title>
            <meta charset=utf-8>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
            <style>
                .heading {
                    text-shadow: #FC0 1px 0 10px;
                }
            </style>
        </head>
        <body>
            <div class="container">
                <h1 class="heading">Posts!</h1>
                <p>$body</p>
            </div>
        </body>
    </html>
    """
    ).safe_substitute(body=body)


def render_posts(posts: list[dict[str, str]]):
    return (
        "<ul>"
        + "".join(["<li>" + html.escape(s["data"]) + "</li>" for s in posts])
        + "</ul>"
    )


def application(environ, start_response):
    start_response("200 ok", [("Content-type", "text/html")])
    post_data = get_all_posts()
    return [
        render_page(render_posts(post_data["storage"])).encode(),
    ]

```

