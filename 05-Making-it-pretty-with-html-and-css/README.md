# step 0
[Let's start where we left off](/04-From-storage-to-web/README.md).
```python
from dys import _chain, get_script_address, get_caller


def say_hello(my_name: str):
    assert get_script_address() == get_caller(), "permission denied"
    data = "hello " + my_name
    print(data)  # to help debug
    return _chain(
        "dyson/sendMsgCreateStorage",
        creator=get_script_address(),
        index=get_script_address() + "/my_greeting",
        data=data,
        force=True,
    )


def get_greeting():
    return _chain(
        "dyson/QueryStorage",
        index=get_script_address() + "/my_greeting",
    )


def application(environ, start_response):
    start_response("200 ok", [("Content-type", "text/plain")])
    greeting_data = get_greeting()["result"]["storage"]["data"]
    return [greeting_data.encode()]
```
# step 1

Sanitize our outputs with [`html.escape`](https://docs.python.org/3/library/html.html#html.escape), this let's us put special charachters like & in our name and it will be encoded for HTML output.
We are also changing the `Content-type` to `text/html` so that the browser knows to render code as HTML and not just text like before.

```diff
@@ -1,3 +1,4 @@
+import html
 from dys import _chain, get_script_address, get_caller
 
 
@@ -22,6 +23,6 @@
 
 
 def application(environ, start_response):
-    start_response("200 ok", [("Content-type", "text/plain")])
+    start_response("200 ok", [("Content-type", "text/html")])
     greeting_data = get_greeting()["result"]["storage"]["data"]
-    return [greeting_data.encode()]
+    return [html.escape(greeting_data).encode()]

```
# step 2

Now we make things a bit more fancy with some minimal (but valid) HTML and a title.

```diff
@@ -25,4 +25,10 @@
 def application(environ, start_response):
     start_response("200 ok", [("Content-type", "text/html")])
     greeting_data = get_greeting()["result"]["storage"]["data"]
-    return [html.escape(greeting_data).encode()]
+    return [
+        b"<!doctype html><meta charset=utf-8><title>Hello World</title>",
+        b"<h1>Greeting!</h1>",
+        b"<p>",
+        html.escape(greeting_data).encode(),
+        b"</p>",
+    ]

```
# step 3
But making the list of bytestrings is not flexible, let's extract the template logic to a function. And render the HTML template using a [f-strings](https://docs.python.org/3/tutorial/inputoutput.html#tut-f-strings).
Its input is a string parameter named `body`. And to help with debuging, you can test this function for free using the "Query render_page" button and looking at the return value.

```diff
@@ -22,13 +22,18 @@
     )
 
 
+def render_page(body:str):
+    return f"""<!doctype html>
+    <meta charset=utf-8>
+    <title>Hello World</title>
+    <h1>Greeting!</h1>
+    <p>{body}</p>
+    """
+
+
 def application(environ, start_response):
     start_response("200 ok", [("Content-type", "text/html")])
     greeting_data = get_greeting()["result"]["storage"]["data"]
     return [
-        b"<!doctype html><meta charset=utf-8><title>Hello World</title>",
-        b"<h1>Greeting!</h1>",
-        b"<p>",
-        html.escape(greeting_data).encode(),
-        b"</p>",
+        render_page(html.escape(greeting_data)).encode(),
     ]

```
# step 4
Now let's make it stylish by using [Bootstrap](https://getbootstrap.com/docs/5.2/getting-started/introduction/) from a CDN
```diff
@@ -24,10 +24,20 @@
 
 def render_page(body:str):
     return f"""<!doctype html>
-    <meta charset=utf-8>
-    <title>Hello World</title>
-    <h1>Greeting!</h1>
-    <p>{body}</p>
+    <html>
+        <head>
+            <title>Hello World</title>
+            <meta charset=utf-8>
+            <meta name="viewport" content="width=device-width, initial-scale=1">
+            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
+        </head>
+        <body>
+            <div class="container">
+                <h1>Greeting!</h1>
+                <p>{body}</p>
+            </div>
+        </body>
+    </html>
     """
 
 

```
# step 5
In the last steps we have been using "format strings" to render the HTML. 
In this step we can see an alternative way to render a string buy using [`string.Template.safe_substitute`](https://docs.python.org/3/library/string.html#string.Template.safe_substitute)
And we are also added a class and [`text-shadow`](https://developer.mozilla.org/en-US/docs/Web/CSS/text-shadow) style to the header element.
```diff
@@ -1,4 +1,5 @@
 import html
+from string import Template
 from dys import _chain, get_script_address, get_caller
 
 
@@ -23,7 +24,8 @@
 
 
 def render_page(body:str):
-    return f"""<!doctype html>
+    return Template(
+        """<!doctype html>
     <html>
         <head>
             <title>Hello World</title>
@@ -34,11 +36,12 @@
         <body>
             <div class="container">
                 <h1>Greeting!</h1>
-                <p>{body}</p>
+                <p>$body</p>
             </div>
         </body>
     </html>
     """
+    ).safe_substitute(body=body)
 
 
 def application(environ, start_response):

```
# step 6
And we can also add a class and [`text-shadow`](https://developer.mozilla.org/en-US/docs/Web/CSS/text-shadow) style to the header element.
```diff
@@ -32,10 +32,15 @@
             <meta charset=utf-8>
             <meta name="viewport" content="width=device-width, initial-scale=1">
             <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
+            <style>
+                .heading {
+                    text-shadow: #FC0 1px 0 10px;
+                }
+            </style>
         </head>
         <body>
             <div class="container">
-                <h1>Greeting!</h1>
+                <h1 class="heading">Greeting!</h1>
                 <p>$body</p>
             </div>
         </body>

```

# Conclusion

In this post we learned:
1. How to render HTML and escape strings for HTML
2. Basics of format strings
3. How to alternatively render strings with `string.Template`
4. How to link to external assets like JS and CSS

Try customizing your page with more css and have fun with it!

```python
import html
from string import Template
from dys import _chain, get_script_address, get_caller


def say_hello(my_name: str):
    assert get_script_address() == get_caller(), "permission denied"
    data = "hello " + my_name
    print(data)  # to help debug
    return _chain(
        "dyson/sendMsgCreateStorage",
        creator=get_script_address(),
        index=get_script_address() + "/my_greeting",
        data=data,
        force=True,
    )


def get_greeting():
    return _chain(
        "dyson/QueryStorage",
        index=get_script_address() + "/my_greeting",
    )


def render_page(body:str):
    return Template(
        """<!doctype html>
    <html>
        <head>
            <title>Hello World</title>
            <meta charset=utf-8>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
            <style>
                .heading {
                    text-shadow: #FC0 1px 0 10px;
                }
            </style>
        </head>
        <body>
            <div class="container">
                <h1 class="heading">Greeting!</h1>
                <p>$body</p>
            </div>
        </body>
    </html>
    """
    ).safe_substitute(body=body)


def application(environ, start_response):
    start_response("200 ok", [("Content-type", "text/html")])
    greeting_data = get_greeting()["result"]["storage"]["data"]
    return [
        render_page(html.escape(greeting_data)).encode(),
    ]

```

