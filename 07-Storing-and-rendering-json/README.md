# step 0
Let's start where we left off [06-Looping-over-storage-simple-blog](/03-Looping-over-storage-simple-blog/README.md).
```python
import re2
import html
from string import Template
from dys import _chain, get_script_address, get_caller


def new_post(title: str, body: str):
    assert get_script_address() == get_caller(), "permission denied"
    data = f"{title} -- {body}"
    title_slug = re2.sub("\W+", "-", title)
    print(data)  # to help debug
    return _chain(
        "dyson/sendMsgCreateStorage",
        creator=get_script_address(),
        index=get_script_address() + "/post/v1/" + title_slug,
        data=data,
        force=True,
    )


def get_all_posts():
    response = _chain(
        "dyson/QueryPrefixStorage", 
        prefix=get_script_address() + "/post/v1/"
    )
    return response["result"]


def render_page(body: str):
    return Template(
        """<!doctype html>
    <html>
        <head>
            <title>Hello World</title>
            <meta charset=utf-8>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
            <style>
                .heading {
                    text-shadow: #FC0 1px 0 10px;
                }
            </style>
        </head>
        <body>
            <div class="container">
                <h1 class="heading">Posts!</h1>
                <p>$body</p>
            </div>
        </body>
    </html>
    """
    ).safe_substitute(body=body)


def render_posts(posts: list[dict[str, str]]):
    return (
        "<ul>"
        + "".join(["<li>" + html.escape(s["data"]) + "</li>" for s in posts])
        + "</ul>"
    )


def application(environ, start_response):
    start_response("200 ok", [("Content-type", "text/html")])
    post_data = get_all_posts()
    return [
        render_page(render_posts(post_data["storage"])).encode(),
    ]
```
# step 1

The posts so far have been a simple string that we are displaying. But this is now a bit too simple for out future plans. 
This step changes the data stored in `new_post` from a simple string, to a JSON dict. It's important to note, the only data that can be stored 
in a Storage object on chain is a string. So we must use `JSON.dumps()`. Dyson doesn't care what is inside the string, only that it is a string.

We also changed `get_all_posts()` to return all posts, both v1 and v2. At this point you should make a few new posts and see the raw JSON 
string displayed in the website.

```diff
@@ -1,3 +1,4 @@
+import json
 import re2
 import html
 from string import Template
@@ -6,13 +7,13 @@
 
 def new_post(title: str, body: str):
     assert get_script_address() == get_caller(), "permission denied"
-    data = f"{title} -- {body}"
+    data = json.dumps({"title": title, "body": body})
     title_slug = re2.sub("\W+", "-", title)
     print(data)  # to help debug
     return _chain(
         "dyson/sendMsgCreateStorage",
         creator=get_script_address(),
-        index=get_script_address() + "/post/v1/" + title_slug,
+        index=get_script_address() + "/post/v2/" + title_slug,
         data=data,
         force=True,
     )
@@ -20,8 +21,7 @@
 
 def get_all_posts():
     response = _chain(
-        "dyson/QueryPrefixStorage", 
-        prefix=get_script_address() + "/post/v1/"
+        "dyson/QueryPrefixStorage", prefix=get_script_address() + "/post/"
     )
     return response["result"]
 

```
# step 2

So we have a situation where we have two differnt dataschemes, the first is "v1" where the post as a string of "title -- body" and the then there is "v2"
where it is a JSON object.

There are many ways to handle this type of situation. What works best for your app will depend on your data and logic. 
In this tutorial we will simply render both types of posts depending on the type of data.

In this step we sperate the render logic of the list of posts `render_posts` from rendering a specific post in `render_post`. 
It is a small change now but the flexibility will help us.

Generally though, you should try to finalize your data structures before using your app in production. 

```diff
@@ -52,10 +52,14 @@
     ).safe_substitute(body=body)
 
 
+def render_post(post: dict[str, str]):
+    return html.escape(post["data"])
+
+
 def render_posts(posts: list[dict[str, str]]):
     return (
         "<ul>"
-        + "".join(["<li>" + html.escape(s["data"]) + "</li>" for s in posts])
+        + "".join(["<li>" + render_post(post) + "</li>" for post in posts])
         + "</ul>"
     )
 

```
# step 3

In this step, we are switching how to render the post depending on if it has `/v1/` or `/v2/` in the index. In this case, the slashes are important.
If we only used `v1` or `v2` without the slashes `/`, then it may cause errors from matching the title or in the script address. 

Now if you have some new posts made and still have old posts from the last tutorial, you should see both rendered properly in the browser.

```diff
@@ -53,7 +53,15 @@
 
 
 def render_post(post: dict[str, str]):
-    return html.escape(post["data"])
+    if "/v1/" in post["index"]:
+        return html.escape(post["data"])
+    if "/v2/" in post["index"]:
+        template = Template("<div><h2>$title</h2><p>$body</p></div>")
+        post_data = json.loads(post["data"])
+        return template.safe_substitute(
+            title=html.escape(post_data["title"]),
+            body=html.escape(post_data["body"]),
+        )
 
 
 def render_posts(posts: list[dict[str, str]]):

```

# Conclusion

In this post we learned:

1. How to store complex JSON objects in the storage
2. How to conditionally render data based on the index version

Try customizing your page with more css and have fun with it!

```python
import json
import re2
import html
from string import Template
from dys import _chain, get_script_address, get_caller


def new_post(title: str, body: str):
    assert get_script_address() == get_caller(), "permission denied"
    data = json.dumps({"title": title, "body": body})
    title_slug = re2.sub("\W+", "-", title)
    print(data)  # to help debug
    return _chain(
        "dyson/sendMsgCreateStorage",
        creator=get_script_address(),
        index=get_script_address() + "/post/v2/" + title_slug,
        data=data,
        force=True,
    )


def get_all_posts():
    response = _chain(
        "dyson/QueryPrefixStorage", prefix=get_script_address() + "/post/"
    )
    return response["result"]


def render_page(body: str):
    return Template(
        """<!doctype html>
    <html>
        <head>
            <title>Hello World</title>
            <meta charset=utf-8>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
            <style>
                .heading {
                    text-shadow: #FC0 1px 0 10px;
                }
            </style>
        </head>
        <body>
            <div class="container">
                <h1 class="heading">Posts!</h1>
                <p>$body</p>
            </div>
        </body>
    </html>
    """
    ).safe_substitute(body=body)


def render_post(post: dict[str, str]):
    if "/v1/" in post["index"]:
        return html.escape(post["data"])
    if "/v2/" in post["index"]:
        template = Template("<div><h2>$title</h2><p>$body</p></div>")
        post_data = json.loads(post["data"])
        return template.safe_substitute(
            title=html.escape(post_data["title"]),
            body=html.escape(post_data["body"]),
        )


def render_posts(posts: list[dict[str, str]]):
    return (
        "<ul>"
        + "".join(["<li>" + render_post(post) + "</li>" for post in posts])
        + "</ul>"
    )


def application(environ, start_response):
    start_response("200 ok", [("Content-type", "text/html")])
    post_data = get_all_posts()
    return [
        render_page(render_posts(post_data["storage"])).encode(),
    ]

```

