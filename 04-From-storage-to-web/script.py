from dys import _chain, get_script_address, get_caller


def say_hello(my_name: str):
    assert get_script_address() == get_caller(), "permission denied"
    data = "hello " + my_name
    print(data)  # to help debug
    return _chain(
        "dyson/sendMsgCreateStorage",
        creator=get_script_address(),
        index=get_script_address() + "/my_greeting",
        data=data,
        force=True,
    )
 
 
def get_greeting():
    return _chain(
        "dyson/QueryStorage",
        index=get_script_address() + "/my_greeting",
    )
 
 
def application(environ, start_response):
    start_response("200 ok", [("Content-type", "text/plain")])
    greeting_data = get_greeting()["result"]["storage"]["data"]
    return [greeting_data.encode()]
