# step 1
[Let's start where we left off](https://medium.com/@dysonprotocol/how-to-save-to-storage-on-dyson-protocol-part-3-of-4-85936bdc2cbc).
```diff
+from dys import _chain, SCRIPT_ADDRESS
+
+
+def say_hello(my_name: str):
+    data = "hello " + my_name
+    print(data)  # to help debug
+    return _chain(
+        "dyson/sendMsgCreateStorage",
+        creator=SCRIPT_ADDRESS,
+        index=SCRIPT_ADDRESS + "/my_greeting",
+        data=data,
+        force=True,
+    )

```
# step 2
update imports to get the script address

```diff
-from dys import _chain, SCRIPT_ADDRESS
+from dys import _chain, get_script_address
 
 
 def say_hello(my_name: str):
     data = "hello " + my_name
     print(data)  # to help debug
     return _chain(
         "dyson/sendMsgCreateStorage",
-        creator=SCRIPT_ADDRESS,
-        index=SCRIPT_ADDRESS + "/my_greeting",
+        creator=get_script_address(),
+        index=get_script_address() + "/my_greeting",
         data=data,
         force=True,
     )

```
# step 3
add acceess control

```diff
-from dys import _chain, get_script_address
+from dys import _chain, get_script_address, get_caller
 
 
 def say_hello(my_name: str):
+    assert get_script_address() == get_caller(), "permission denied"
     data = "hello " + my_name
     print(data)  # to help debug
     return _chain(
         "dyson/sendMsgCreateStorage",
         creator=get_script_address(),
         index=get_script_address() + "/my_greeting",
         data=data,
         force=True,
     )

```
# step 4
add getter for the greeting storage

```diff
 from dys import _chain, get_script_address, get_caller
 
 
 def say_hello(my_name: str):
     assert get_script_address() == get_caller(), "permission denied"
     data = "hello " + my_name
     print(data)  # to help debug
     return _chain(
         "dyson/sendMsgCreateStorage",
         creator=get_script_address(),
         index=get_script_address() + "/my_greeting",
         data=data,
         force=True,
     )
+
+
+def get_greeting():
+    pass

```
# step 5
use `dyson/QueryStorage` query in `get_greeting()` to get the data

```diff
 from dys import _chain, get_script_address, get_caller
 
 
 def say_hello(my_name: str):
     assert get_script_address() == get_caller(), "permission denied"
     data = "hello " + my_name
     print(data)  # to help debug
     return _chain(
         "dyson/sendMsgCreateStorage",
         creator=get_script_address(),
         index=get_script_address() + "/my_greeting",
         data=data,
         force=True,
     )
 
 
 def get_greeting():
-    pass
+    return _chain(
+        "dyson/QueryStorage",
+        index=get_script_address() + "/my_greeting",
+    )

```
# step 6
Let us add simple [wsgi application](https://wsgi.readthedocs.io/en/latest/learn.html) for browser. After saving this go to https://[YOUR SCRIPT ADDRESS HERE].dysonvalidator.com/ 

```diff
 from dys import _chain, get_script_address, get_caller
 
 
 def say_hello(my_name: str):
     assert get_script_address() == get_caller(), "permission denied"
     data = "hello " + my_name
     print(data)  # to help debug
     return _chain(
         "dyson/sendMsgCreateStorage",
         creator=get_script_address(),
         index=get_script_address() + "/my_greeting",
         data=data,
         force=True,
     )
 
 
 def get_greeting():
     return _chain(
         "dyson/QueryStorage",
         index=get_script_address() + "/my_greeting",
     )
+
+
+def application(environ, start_response):
+    start_response("200 ok", [("Content-type", "text/plain")])
+    return ["Test WSGI website says: hello world"]

```
# step 7

Oh no last step caused an error:

```
Error: write() argument must be a bytes instance on line: None col: None

Logs:
```
The WSGI app must return bytes not strings, in this case use str.encode() to encode it to bytes.

```diff
 from dys import _chain, get_script_address, get_caller
 
 
 def say_hello(my_name: str):
     assert get_script_address() == get_caller(), "permission denied"
     data = "hello " + my_name
     print(data)  # to help debug
     return _chain(
         "dyson/sendMsgCreateStorage",
         creator=get_script_address(),
         index=get_script_address() + "/my_greeting",
         data=data,
         force=True,
     )
 
 
 def get_greeting():
     return _chain(
         "dyson/QueryStorage",
         index=get_script_address() + "/my_greeting",
     )
 
 
 def application(environ, start_response):
     start_response("200 ok", [("Content-type", "text/plain")])
-    return ["Test WSGI website says: hello world"]
+    return ["Test WSGI website says: hello world".encode()]

```
# step 8
Now we can call `get_greeting()` in the WSGI app and see our name in the browser. Good job!

In this tutorial we learned:
1. A simple example of how to enforce access control
2. How to read Storage from the chain in a function
3. How to make a simple WSGI app
4. And how to call a function and render it to the browser.


```diff
 from dys import _chain, get_script_address, get_caller
 
 
 def say_hello(my_name: str):
     assert get_script_address() == get_caller(), "permission denied"
     data = "hello " + my_name
     print(data)  # to help debug
     return _chain(
         "dyson/sendMsgCreateStorage",
         creator=get_script_address(),
         index=get_script_address() + "/my_greeting",
         data=data,
         force=True,
     )
 
 
 def get_greeting():
     return _chain(
         "dyson/QueryStorage",
         index=get_script_address() + "/my_greeting",
     )
 
 
 def application(environ, start_response):
     start_response("200 ok", [("Content-type", "text/plain")])
-    return ["Test WSGI website says: hello world".encode()]
+    greeting_data = get_greeting()["result"]["storage"]["data"]
+    return [greeting_data.encode()]

```
