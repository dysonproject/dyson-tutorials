

--------

# STEP 0

start from the last post.

Note: you may get `MemoryError('Out of Gas')`. That's because gas estimates are dynamic now and start much much lower than before to save you DYS.
The function will automatically adjust the gas up and down so just run the function again if you see this error.


### Source 0
```python
import json
import re2
import html
from string import Template
from dys import _chain, get_script_address, get_caller


def new_post(title: str, body: str):
    assert get_script_address() == get_caller(), "permission denied"
    data = json.dumps({"title": title, "body": body})
    title_slug = re2.sub("\W+", "-", title)
    print(data)  # to help debug
    return _chain(
        "dyson/sendMsgCreateStorage",
        creator=get_script_address(),
        index=get_script_address() + "/post/v2/" + title_slug,
        data=data,
        force=True,
    )


def get_all_posts():
    response = _chain(
        "dyson/QueryPrefixStorage", prefix=get_script_address() + "/post/"
    )
    return response["result"]


def render_page(body: str):
    return Template(
        """<!doctype html>
    <html>
        <head>
            <title>Hello World</title>
            <meta charset=utf-8>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
            <style>
                .heading {
                    text-shadow: #FC0 1px 0 10px;
                }
            </style>
        </head>
        <body>
            <div class="container">
                <h1 class="heading">Posts!</h1>
                <p>$body</p>
            </div>
        </body>
    </html>
    """
    ).safe_substitute(body=body)


def render_post(post: dict[str, str]):
    if "/v1/" in post["index"]:
        return html.escape(post["data"])
    if "/v2/" in post["index"]:
        template = Template("<div><h2>$title</h2><p>$body</p></div>")
        post_data = json.loads(post["data"])
        return template.safe_substitute(
            title=html.escape(post_data["title"]),
            body=html.escape(post_data["body"]),
        )


def render_posts(posts: list[dict[str, str]]):
    return (
        "<ul>"
        + "".join(["<li>" + render_post(post) + "</li>" for post in posts])
        + "</ul>"
    )


def application(environ, start_response):
    start_response("200 ok", [("Content-type", "text/html")])
    post_data = get_all_posts()
    return [
        render_page(render_posts(post_data["storage"])).encode(),
    ]
```


--------

# STEP 1

we will use the command `dyson/sendMsgCreateSchedualedRun` to run a function in a script at a specific block height.

This function looks like it is doing a lot, so be sure to read the comments to see what each line does. 

We can optimize for gas by combining operations into a single lines but having it seperate is nice for development and learning what each line does.

This is the query to see all the scheduled runs for a script, use your script address to see this last schduled run.
It will  be at the top because we use `reverse` in the pagination. By default the list is from older -> newest.

https://dys.dysonvalidator.com/commands?command=dyson%2FQueryScheduledRunAll&data=%7B%22index%22%3A%22your+address%22%2C%22pagination%22%3A%7B%22reverse%22%3Atrue%7D%7D

Gas for schedualed runs starts at 10x the cost of normal script runs to prevent spam. So try to do as little computation as possible.

```
{
  "scheduled_run": [
    {
      "index": "dys1enyja0fe7d92lkuuzdss6ytp5e6pfkz3pqdckz/000001346121/1",
      "creator": "dys1enyja0fe7d92lkuuzdss6ytp5e6pfkz3pqdckz",
      "height": "1346121",
      "gas": "200000",
      "msg": {
        "creator": "dys1enyja0fe7d92lkuuzdss6ytp5e6pfkz3pqdckz",
        "address": "dys1enyja0fe7d92lkuuzdss6ytp5e6pfkz3pqdckz",
        "extra_lines": "",
        "function_name": "new_post",
        "args": "",
        "kwargs": "{"title": "The ! title (new post in the future)", "body": "2 this is the future!"}",
        "coins": ""
      },
      "resp": {   < ---- THIS IS EMPTY UNTIL IT IS RUN
        "response": "{"cumsize": 54255, "exception": null, "gas_limit": 200000, "nodes_called": 77, "result": {"error": null, "id": 0, "result": {}}, "script_gas_consumed": 73962, "stdout": "{\"title\": \"The ! title (new post in the future)\", \"body\": \"2 this is the future!\"}\n"}"
      },
      "error": "",
      "gasprice": {  <-- how much gas costs
        "denom": "dys",
        "amount": "0.010000000000000000"
      },
      "fee": {
        "denom": "dys",
        "amount": "2000"
      }
    },
```


### DIFF 1
```diff
@@ -1,80 +1,109 @@
 import json
 import re2
 import html
 from string import Template
-from dys import _chain, get_script_address, get_caller
+from dys import _chain, get_script_address, get_caller, get_block_info
+
+
+def schedule_new_post(blocks_in_the_future: int, gas: int, title: str, body: str):
+    # store in a variable because we will need this value several times
+    address = get_script_address()
+
+    # access control
+    assert address == get_caller(), "permission denied"
+
+    # get the current height of this transaction
+    current_height = get_block_info()["height"]
+    future_block_height = current_height + blocks_in_the_future
+
+    # construct the message to call "new_post"
+    msg = {
+        "creator": address,
+        "address": address,
+        "function_name": "new_post",
+        "kwargs": json.dumps({"title": title, "body": body}),
+    }
+
+    return _chain(
+        "dyson/sendMsgCreateScheduledRun",
+        creator=address,
+        # schedule the function for the blocks in the future
+        height=future_block_height,
+        gas=gas,
+        msg=msg,
+    )
 
 
 def new_post(title: str, body: str):
     assert get_script_address() == get_caller(), "permission denied"
     data = json.dumps({"title": title, "body": body})
     title_slug = re2.sub("\W+", "-", title)
     print(data)  # to help debug
     return _chain(
         "dyson/sendMsgCreateStorage",
         creator=get_script_address(),
         index=get_script_address() + "/post/v2/" + title_slug,
         data=data,
         force=True,
     )
 
 
 def get_all_posts():
     response = _chain(
         "dyson/QueryPrefixStorage", prefix=get_script_address() + "/post/"
     )
     return response["result"]
 
 
 def render_page(body: str):
     return Template(
         """<!doctype html>
     <html>
         <head>
             <title>Hello World</title>
             <meta charset=utf-8>
             <meta name="viewport" content="width=device-width, initial-scale=1">
             <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
             <style>
                 .heading {
                     text-shadow: #FC0 1px 0 10px;
                 }
             </style>
         </head>
         <body>
             <div class="container">
                 <h1 class="heading">Posts!</h1>
                 <p>$body</p>
             </div>
         </body>
     </html>
     """
     ).safe_substitute(body=body)
 
 
 def render_post(post: dict[str, str]):
     if "/v1/" in post["index"]:
         return html.escape(post["data"])
     if "/v2/" in post["index"]:
         template = Template("<div><h2>$title</h2><p>$body</p></div>")
         post_data = json.loads(post["data"])
         return template.safe_substitute(
             title=html.escape(post_data["title"]),
             body=html.escape(post_data["body"]),
         )
 
 
 def render_posts(posts: list[dict[str, str]]):
     return (
         "<ul>"
         + "".join(["<li>" + render_post(post) + "</li>" for post in posts])
         + "</ul>"
     )
 
 
 def application(environ, start_response):
     start_response("200 ok", [("Content-type", "text/html")])
     post_data = get_all_posts()
     return [
         render_page(render_posts(post_data["storage"])).encode(),
     ]

```
### Source 1
```python

import json
import re2
import html
from string import Template
from dys import _chain, get_script_address, get_caller, get_block_info


def schedule_new_post(blocks_in_the_future: int, gas: int, title: str, body: str):
    # store in a variable because we will need this value several times
    address = get_script_address()

    # access control
    assert address == get_caller(), "permission denied"

    # get the current height of this transaction
    current_height = get_block_info()["height"]
    future_block_height = current_height + blocks_in_the_future

    # construct the message to call "new_post"
    msg = {
        "creator": address,
        "address": address,
        "function_name": "new_post",
        "kwargs": json.dumps({"title": title, "body": body}),
    }

    return _chain(
        "dyson/sendMsgCreateScheduledRun",
        creator=address,
        # schedule the function for the blocks in the future
        height=future_block_height,
        gas=gas,
        msg=msg,
    )


def new_post(title: str, body: str):
    assert get_script_address() == get_caller(), "permission denied"
    data = json.dumps({"title": title, "body": body})
    title_slug = re2.sub("\W+", "-", title)
    print(data)  # to help debug
    return _chain(
        "dyson/sendMsgCreateStorage",
        creator=get_script_address(),
        index=get_script_address() + "/post/v2/" + title_slug,
        data=data,
        force=True,
    )


def get_all_posts():
    response = _chain(
        "dyson/QueryPrefixStorage", prefix=get_script_address() + "/post/"
    )
    return response["result"]


def render_page(body: str):
    return Template(
        """<!doctype html>
    <html>
        <head>
            <title>Hello World</title>
            <meta charset=utf-8>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
            <style>
                .heading {
                    text-shadow: #FC0 1px 0 10px;
                }
            </style>
        </head>
        <body>
            <div class="container">
                <h1 class="heading">Posts!</h1>
                <p>$body</p>
            </div>
        </body>
    </html>
    """
    ).safe_substitute(body=body)


def render_post(post: dict[str, str]):
    if "/v1/" in post["index"]:
        return html.escape(post["data"])
    if "/v2/" in post["index"]:
        template = Template("<div><h2>$title</h2><p>$body</p></div>")
        post_data = json.loads(post["data"])
        return template.safe_substitute(
            title=html.escape(post_data["title"]),
            body=html.escape(post_data["body"]),
        )


def render_posts(posts: list[dict[str, str]]):
    return (
        "<ul>"
        + "".join(["<li>" + render_post(post) + "</li>" for post in posts])
        + "</ul>"
    )


def application(environ, start_response):
    start_response("200 ok", [("Content-type", "text/html")])
    post_data = get_all_posts()
    return [
        render_page(render_posts(post_data["storage"])).encode(),
    ]




```



--------

# STEP 2

Now let's fix the sorting of our posts.

Originally they are sorted alphabetically, but it will be more intuitive for readers if they are sorted by time.

Most of the work will be done in the new function `new_title_path`.
This will be used to build the index. The most important part is `get_block_info()['height']`.

Because this is a blockchain, the block height is incresing monotomically. We will insert the bloch height in the index
before the title, which will give allow the order to be maintainted regardless of the title.

The function `slugify` is created to normalize the title for use in a URL path. Currently all posts are 
displayed in the main page, but it would be good to be able to link to a specific post. So this function
is preparing for that feature in the future.

Also we are updating the render post to see that actual index of each post, it's not a link yet, but it 
is useful if we want to look up a post to delete [link to last article] like we learned in the last post.


### DIFF 2
```diff
@@ -1,109 +1,141 @@
 import json
 import re2
 import html
 from string import Template
 from dys import _chain, get_script_address, get_caller, get_block_info
+import urllib.parse
 
 
 def schedule_new_post(blocks_in_the_future: int, gas: int, title: str, body: str):
     # store in a variable because we will need this value several times
     address = get_script_address()
 
     # access control
     assert address == get_caller(), "permission denied"
 
     # get the current height of this transaction
     current_height = get_block_info()["height"]
     future_block_height = current_height + blocks_in_the_future
 
     # construct the message to call "new_post"
     msg = {
         "creator": address,
         "address": address,
         "function_name": "new_post",
         "kwargs": json.dumps({"title": title, "body": body}),
     }
 
     return _chain(
         "dyson/sendMsgCreateScheduledRun",
         creator=address,
         # schedule the function for the blocks in the future
         height=future_block_height,
         gas=gas,
         msg=msg,
     )
 
 
+def slugify(title: str):
+    # convert to lowercase
+    # "The Post!!" -> "the post!!"
+    title = title.lower()
+
+    # replace special characters with " " space
+    # "the post!!" -> "the post  "
+    title = re2.sub("\W+", " ", title)
+
+    # split on spaces into a list
+    # "the post  " -> ["the", "post"]
+    title = title.split()
+
+    # join the list with "-" dashes
+    # ["the", "post"] -> "the-post"
+    title = "-".join(title)
+    return title
+
+
+def new_title_path(title: str):
+    title_slug = slugify(title)
+    return f"/post/v2/{get_block_info()['height']:010d}/{title_slug}"
+
+
 def new_post(title: str, body: str):
     assert get_script_address() == get_caller(), "permission denied"
     data = json.dumps({"title": title, "body": body})
-    title_slug = re2.sub("\W+", "-", title)
-    print(data)  # to help debug
     return _chain(
         "dyson/sendMsgCreateStorage",
         creator=get_script_address(),
-        index=get_script_address() + "/post/v2/" + title_slug,
+        index=get_script_address() + new_title_path(title),
         data=data,
         force=True,
     )
 
 
 def get_all_posts():
     response = _chain(
         "dyson/QueryPrefixStorage", prefix=get_script_address() + "/post/"
     )
     return response["result"]
 
 
 def render_page(body: str):
     return Template(
         """<!doctype html>
     <html>
         <head>
             <title>Hello World</title>
             <meta charset=utf-8>
             <meta name="viewport" content="width=device-width, initial-scale=1">
             <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
             <style>
                 .heading {
                     text-shadow: #FC0 1px 0 10px;
                 }
             </style>
         </head>
         <body>
             <div class="container">
                 <h1 class="heading">Posts!</h1>
                 <p>$body</p>
             </div>
         </body>
     </html>
     """
     ).safe_substitute(body=body)
 
 
 def render_post(post: dict[str, str]):
     if "/v1/" in post["index"]:
         return html.escape(post["data"])
     if "/v2/" in post["index"]:
-        template = Template("<div><h2>$title</h2><p>$body</p></div>")
+        template = Template(
+            """
+            <div>
+                <h2>$title</h2>
+                <small>$index</small>
+                <p>$body</p>
+            </div>
+            """
+        )
         post_data = json.loads(post["data"])
         return template.safe_substitute(
             title=html.escape(post_data["title"]),
             body=html.escape(post_data["body"]),
+            index=urllib.parse.quote(post["index"], safe=":/"),
         )
 
 
 def render_posts(posts: list[dict[str, str]]):
     return (
         "<ul>"
         + "".join(["<li>" + render_post(post) + "</li>" for post in posts])
         + "</ul>"
     )
 
 
 def application(environ, start_response):
     start_response("200 ok", [("Content-type", "text/html")])
     post_data = get_all_posts()
     return [
         render_page(render_posts(post_data["storage"])).encode(),
     ]

```
### Source 2
```python

import json
import re2
import html
from string import Template
from dys import _chain, get_script_address, get_caller, get_block_info
import urllib.parse


def schedule_new_post(blocks_in_the_future: int, gas: int, title: str, body: str):
    # store in a variable because we will need this value several times
    address = get_script_address()

    # access control
    assert address == get_caller(), "permission denied"

    # get the current height of this transaction
    current_height = get_block_info()["height"]
    future_block_height = current_height + blocks_in_the_future

    # construct the message to call "new_post"
    msg = {
        "creator": address,
        "address": address,
        "function_name": "new_post",
        "kwargs": json.dumps({"title": title, "body": body}),
    }

    return _chain(
        "dyson/sendMsgCreateScheduledRun",
        creator=address,
        # schedule the function for the blocks in the future
        height=future_block_height,
        gas=gas,
        msg=msg,
    )


def slugify(title: str):
    # convert to lowercase
    # "The Post!!" -> "the post!!"
    title = title.lower()

    # replace special characters with " " space
    # "the post!!" -> "the post  "
    title = re2.sub("\W+", " ", title)

    # split on spaces into a list
    # "the post  " -> ["the", "post"]
    title = title.split()

    # join the list with "-" dashes
    # ["the", "post"] -> "the-post"
    title = "-".join(title)
    return title


def new_title_path(title: str):
    title_slug = slugify(title)
    return f"/post/v2/{get_block_info()['height']:010d}/{title_slug}"


def new_post(title: str, body: str):
    assert get_script_address() == get_caller(), "permission denied"
    data = json.dumps({"title": title, "body": body})
    return _chain(
        "dyson/sendMsgCreateStorage",
        creator=get_script_address(),
        index=get_script_address() + new_title_path(title),
        data=data,
        force=True,
    )


def get_all_posts():
    response = _chain(
        "dyson/QueryPrefixStorage", prefix=get_script_address() + "/post/"
    )
    return response["result"]


def render_page(body: str):
    return Template(
        """<!doctype html>
    <html>
        <head>
            <title>Hello World</title>
            <meta charset=utf-8>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
            <style>
                .heading {
                    text-shadow: #FC0 1px 0 10px;
                }
            </style>
        </head>
        <body>
            <div class="container">
                <h1 class="heading">Posts!</h1>
                <p>$body</p>
            </div>
        </body>
    </html>
    """
    ).safe_substitute(body=body)


def render_post(post: dict[str, str]):
    if "/v1/" in post["index"]:
        return html.escape(post["data"])
    if "/v2/" in post["index"]:
        template = Template(
            """
            <div>
                <h2>$title</h2>
                <small>$index</small>
                <p>$body</p>
            </div>
            """
        )
        post_data = json.loads(post["data"])
        return template.safe_substitute(
            title=html.escape(post_data["title"]),
            body=html.escape(post_data["body"]),
            index=urllib.parse.quote(post["index"], safe=":/"),
        )


def render_posts(posts: list[dict[str, str]]):
    return (
        "<ul>"
        + "".join(["<li>" + render_post(post) + "</li>" for post in posts])
        + "</ul>"
    )


def application(environ, start_response):
    start_response("200 ok", [("Content-type", "text/html")])
    post_data = get_all_posts()
    return [
        render_page(render_posts(post_data["storage"])).encode(),
    ]


```



--------

# STEP 3

Now let's add some small improvements to the CSS, becuse we are using bootstrap [link] it's easy to
add some simple styling to our posts.

In `render_posts` we can add the classes `list-group` and `list-group-item`
https://getbootstrap.com/docs/5.0/components/list-group/

And in `render_post` we can add some margin to the bottom of the elements. 


### DIFF 3
```diff
@@ -1,141 +1,144 @@
 import json
 import re2
 import html
 from string import Template
 from dys import _chain, get_script_address, get_caller, get_block_info
 import urllib.parse
 
 
 def schedule_new_post(blocks_in_the_future: int, gas: int, title: str, body: str):
     # store in a variable because we will need this value several times
     address = get_script_address()
 
     # access control
     assert address == get_caller(), "permission denied"
 
     # get the current height of this transaction
     current_height = get_block_info()["height"]
     future_block_height = current_height + blocks_in_the_future
 
     # construct the message to call "new_post"
     msg = {
         "creator": address,
         "address": address,
         "function_name": "new_post",
         "kwargs": json.dumps({"title": title, "body": body}),
     }
 
     return _chain(
         "dyson/sendMsgCreateScheduledRun",
         creator=address,
         # schedule the function for the blocks in the future
         height=future_block_height,
         gas=gas,
         msg=msg,
     )
 
 
 def slugify(title: str):
     # convert to lowercase
     # "The Post!!" -> "the post!!"
     title = title.lower()
 
     # replace special characters with " " space
     # "the post!!" -> "the post  "
     title = re2.sub("\W+", " ", title)
 
     # split on spaces into a list
     # "the post  " -> ["the", "post"]
     title = title.split()
 
     # join the list with "-" dashes
     # ["the", "post"] -> "the-post"
     title = "-".join(title)
     return title
 
 
 def new_title_path(title: str):
     title_slug = slugify(title)
     return f"/post/v2/{get_block_info()['height']:010d}/{title_slug}"
 
 
 def new_post(title: str, body: str):
     assert get_script_address() == get_caller(), "permission denied"
     data = json.dumps({"title": title, "body": body})
     return _chain(
         "dyson/sendMsgCreateStorage",
         creator=get_script_address(),
         index=get_script_address() + new_title_path(title),
         data=data,
         force=True,
     )
 
 
 def get_all_posts():
     response = _chain(
         "dyson/QueryPrefixStorage", prefix=get_script_address() + "/post/"
     )
     return response["result"]
 
 
 def render_page(body: str):
     return Template(
         """<!doctype html>
     <html>
         <head>
             <title>Hello World</title>
             <meta charset=utf-8>
             <meta name="viewport" content="width=device-width, initial-scale=1">
             <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
             <style>
                 .heading {
                     text-shadow: #FC0 1px 0 10px;
                 }
             </style>
         </head>
         <body>
             <div class="container">
                 <h1 class="heading">Posts!</h1>
-                <p>$body</p>
+                <div class="row">$body</div>
             </div>
         </body>
     </html>
     """
     ).safe_substitute(body=body)
 
 
 def render_post(post: dict[str, str]):
     if "/v1/" in post["index"]:
         return html.escape(post["data"])
     if "/v2/" in post["index"]:
         template = Template(
             """
-            <div>
-                <h2>$title</h2>
-                <small>$index</small>
-                <p>$body</p>
-            </div>
+            <h5 class="mb-1">$title</h5>
+            <p class="mb-1">$body</p>
+            <small>$index</small>
             """
         )
         post_data = json.loads(post["data"])
         return template.safe_substitute(
             title=html.escape(post_data["title"]),
             body=html.escape(post_data["body"]),
             index=urllib.parse.quote(post["index"], safe=":/"),
         )
 
 
 def render_posts(posts: list[dict[str, str]]):
     return (
-        "<ul>"
-        + "".join(["<li>" + render_post(post) + "</li>" for post in posts])
+        '<ul class="list-group">'
+        + "".join(
+            [
+                '<li class="list-group-item">' + render_post(post) + "</li>"
+                for post in posts
+            ]
+        )
         + "</ul>"
     )
 
 
 def application(environ, start_response):
     start_response("200 ok", [("Content-type", "text/html")])
     post_data = get_all_posts()
     return [
         render_page(render_posts(post_data["storage"])).encode(),
     ]

```
### Source 3
```python

import json
import re2
import html
from string import Template
from dys import _chain, get_script_address, get_caller, get_block_info
import urllib.parse


def schedule_new_post(blocks_in_the_future: int, gas: int, title: str, body: str):
    # store in a variable because we will need this value several times
    address = get_script_address()

    # access control
    assert address == get_caller(), "permission denied"

    # get the current height of this transaction
    current_height = get_block_info()["height"]
    future_block_height = current_height + blocks_in_the_future

    # construct the message to call "new_post"
    msg = {
        "creator": address,
        "address": address,
        "function_name": "new_post",
        "kwargs": json.dumps({"title": title, "body": body}),
    }

    return _chain(
        "dyson/sendMsgCreateScheduledRun",
        creator=address,
        # schedule the function for the blocks in the future
        height=future_block_height,
        gas=gas,
        msg=msg,
    )


def slugify(title: str):
    # convert to lowercase
    # "The Post!!" -> "the post!!"
    title = title.lower()

    # replace special characters with " " space
    # "the post!!" -> "the post  "
    title = re2.sub("\W+", " ", title)

    # split on spaces into a list
    # "the post  " -> ["the", "post"]
    title = title.split()

    # join the list with "-" dashes
    # ["the", "post"] -> "the-post"
    title = "-".join(title)
    return title


def new_title_path(title: str):
    title_slug = slugify(title)
    return f"/post/v2/{get_block_info()['height']:010d}/{title_slug}"


def new_post(title: str, body: str):
    assert get_script_address() == get_caller(), "permission denied"
    data = json.dumps({"title": title, "body": body})
    return _chain(
        "dyson/sendMsgCreateStorage",
        creator=get_script_address(),
        index=get_script_address() + new_title_path(title),
        data=data,
        force=True,
    )


def get_all_posts():
    response = _chain(
        "dyson/QueryPrefixStorage", prefix=get_script_address() + "/post/"
    )
    return response["result"]


def render_page(body: str):
    return Template(
        """<!doctype html>
    <html>
        <head>
            <title>Hello World</title>
            <meta charset=utf-8>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
            <style>
                .heading {
                    text-shadow: #FC0 1px 0 10px;
                }
            </style>
        </head>
        <body>
            <div class="container">
                <h1 class="heading">Posts!</h1>
                <div class="row">$body</div>
            </div>
        </body>
    </html>
    """
    ).safe_substitute(body=body)


def render_post(post: dict[str, str]):
    if "/v1/" in post["index"]:
        return html.escape(post["data"])
    if "/v2/" in post["index"]:
        template = Template(
            """
            <h5 class="mb-1">$title</h5>
            <p class="mb-1">$body</p>
            <small>$index</small>
            """
        )
        post_data = json.loads(post["data"])
        return template.safe_substitute(
            title=html.escape(post_data["title"]),
            body=html.escape(post_data["body"]),
            index=urllib.parse.quote(post["index"], safe=":/"),
        )


def render_posts(posts: list[dict[str, str]]):
    return (
        '<ul class="list-group">'
        + "".join(
            [
                '<li class="list-group-item">' + render_post(post) + "</li>"
                for post in posts
            ]
        )
        + "</ul>"
    )


def application(environ, start_response):
    start_response("200 ok", [("Content-type", "text/html")])
    post_data = get_all_posts()
    return [
        render_page(render_posts(post_data["storage"])).encode(),
    ]



```


# Conclusion

In this post we learned:
1. how use use `sendMsgCreateSchedualedRun` to schedule running a post at a future block
2. How to use the block height to keep our posts in chronological order
3. How to normalize our title into a slug suitable to url paths 
4. Some more basic styling to improve your posts

Try customizing your page with more css and have fun with it!

```python
import json
import re2
import html
from string import Template
from dys import _chain, get_script_address, get_caller, get_block_info
import urllib.parse


def schedule_new_post(blocks_in_the_future: int, gas: int, title: str, body: str):
    # store in a variable because we will need this value several times
    address = get_script_address()

    # access control
    assert address == get_caller(), "permission denied"

    # get the current height of this transaction
    current_height = get_block_info()["height"]
    future_block_height = current_height + blocks_in_the_future

    # construct the message to call "new_post"
    msg = {
        "creator": address,
        "address": address,
        "function_name": "new_post",
        "kwargs": json.dumps({"title": title, "body": body}),
    }

    return _chain(
        "dyson/sendMsgCreateScheduledRun",
        creator=address,
        # schedule the function for the blocks in the future
        height=future_block_height,
        gas=gas,
        msg=msg,
    )


def slugify(title: str):
    # convert to lowercase
    # "The Post!!" -> "the post!!"
    title = title.lower()

    # replace special characters with " " space
    # "the post!!" -> "the post  "
    title = re2.sub("\W+", " ", title)

    # split on spaces into a list
    # "the post  " -> ["the", "post"]
    title = title.split()

    # join the list with "-" dashes
    # ["the", "post"] -> "the-post"
    title = "-".join(title)
    return title


def new_title_path(title: str):
    title_slug = slugify(title)
    return f"/post/v2/{get_block_info()['height']:010d}/{title_slug}"


def new_post(title: str, body: str):
    assert get_script_address() == get_caller(), "permission denied"
    data = json.dumps({"title": title, "body": body})
    return _chain(
        "dyson/sendMsgCreateStorage",
        creator=get_script_address(),
        index=get_script_address() + new_title_path(title),
        data=data,
        force=True,
    )


def get_all_posts():
    response = _chain(
        "dyson/QueryPrefixStorage", prefix=get_script_address() + "/post/"
    )
    return response["result"]


def render_page(body: str):
    return Template(
        """<!doctype html>
    <html>
        <head>
            <title>Hello World</title>
            <meta charset=utf-8>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
            <style>
                .heading {
                    text-shadow: #FC0 1px 0 10px;
                }
            </style>
        </head>
        <body>
            <div class="container">
                <h1 class="heading">Posts!</h1>
                <div class="row">$body</div>
            </div>
        </body>
    </html>
    """
    ).safe_substitute(body=body)


def render_post(post: dict[str, str]):
    if "/v1/" in post["index"]:
        return html.escape(post["data"])
    if "/v2/" in post["index"]:
        template = Template(
            """
            <h5 class="mb-1">$title</h5>
            <p class="mb-1">$body</p>
            <small>$index</small>
            """
        )
        post_data = json.loads(post["data"])
        return template.safe_substitute(
            title=html.escape(post_data["title"]),
            body=html.escape(post_data["body"]),
            index=urllib.parse.quote(post["index"], safe=":/"),
        )


def render_posts(posts: list[dict[str, str]]):
    return (
        '<ul class="list-group">'
        + "".join(
            [
                '<li class="list-group-item">' + render_post(post) + "</li>"
                for post in posts
            ]
        )
        + "</ul>"
    )


def application(environ, start_response):
    start_response("200 ok", [("Content-type", "text/html")])
    post_data = get_all_posts()
    return [
        render_page(render_posts(post_data["storage"])).encode(),
    ]

```

